class GameView implements View {
	private Level level;
	private TopBar topBar;
	private int backgroundImage;
	
	public GameView(GameManager gameManager) {
        this.topBar = new TopBar(gameManager);
        
        Rect levelBounds = new Rect(0, topBar.height(), width, height - topBar.height());
        
		this.level = new Level(gameManager, levelBounds);
		
		this.backgroundImage = resourceManager.loadResource("../processing/background.png");
	}
	
	void draw() {
		resourceManager.drawImage(backgroundImage, new Rect(
				0,
				0,
				width,
				height));
		
		level.update();
		level.draw();
		this.topBar.draw();
	}
}