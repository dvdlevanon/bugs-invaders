public final static int BULLET_TRAJECTORY_UP    = -10;
public final static int BULLET_TRAJECTORY_DOWN  = 3;
public final static int MAX_BULLETS = 3;

class BulletsManager
{
  private int bulletResourceId;
  private int liveBullets = 0;
  private ArrayList<Bullet> bullets;
  private ArrayList<Bullet> deadBullets;
  private ArrayList<GameObject> gameObjects;
  private Bullet monsterBullet = null;
  private Rect screen;
  private GameObject player;
  
  int count = 100;
  
  public BulletsManager(ArrayList<GameObject> gameObjects, Rect screen, int bulletResourceId)
  {
    this.bullets = new ArrayList<Bullet>();
    this.deadBullets = new ArrayList<Bullet>();
    this.gameObjects = gameObjects;
    this.screen = screen;
    this.bulletResourceId = bulletResourceId;
  }
  
  public void update() {
    for (Bullet bullet : bullets) {
      bullet.update();
      
      for (GameObject object : gameObjects)
      {
        if (bullet.didCollide(object))
        {
          deadBullets.add(bullet);
          object.kill();
          break;
        }
      }
      
      if (!screen.isInside(bullet.position))
      {
        deadBullets.add(bullet);
      }
    }
    
    if (monsterBullet != null)
    {
      monsterBullet.update();
    }

    shotMonsterBullet();
    
    if ((monsterBullet != null) && (player != null))
    {
      if (monsterBullet.didCollide(player))
      {
        monsterBullet = null;
        player.kill();
        player = null;
      }
    }
  }

  public void draw() {
    for (GameObject bullet : bullets)
    {
      bullet.draw();
    }
    
    for (Bullet bullet : deadBullets)
    {
      bullets.remove(bullet);
    }
    
    liveBullets -= deadBullets.size();
    
    deadBullets.clear();
    
    if (monsterBullet != null)
    {
      monsterBullet.draw();
    }
  }
  
  public void addBullet(Rect position, int trajectory)
  {
    if (liveBullets + 1 <= MAX_BULLETS)
    {
      liveBullets++;
      
      Bullet bullet = new Bullet(bulletResourceId, position, trajectory);
      
      bullets.add(bullet);
    }
  }
  
  public void shotMonsterBullet()
  {
    if (count == 0)
    {
      monsterBullet = null;
      count = 200;
    
      if (monsterBullet != null)
      {
        return;
      }
      
      if (player == null) {
        return;
      }
      
      int monsterId = (int)Math.floor((Math.random() * gameObjects.size() - 1) + 1);
      
      Rect monsterPos = gameObjects.get(monsterId).position;
      
      monsterBullet = new Bullet(bulletResourceId, new Rect(monsterPos.x + (monsterPos.width / 2), monsterPos.y, 8, 9), BULLET_TRAJECTORY_DOWN);
    }
    
    count--;
  }
  
  public void addPlayer(GameObject obj)
  {
    player = obj;
  }
}