public static final int KEYCODE_LEFT = 37;
public static final int KEYCODE_RIGHT = 39;
public static final int KEYCODE_SPACE = 32;

class InputManager
{
  private final boolean[] locked = new boolean[256];
  private final boolean[] keyDown = new boolean[256];
  private int[] keyCodes = {};

  // if pressed, and part of our known keyset, mark key as "down"
  private void setIfTrue(int mark, int target) {
    if(!locked[target]) {
      if(mark==target) {
        keyDown[target] = true; }}}

  // if released, and part of our known keyset, mark key as "released"
  private void unsetIfTrue(int mark, int target) {
    if(mark==target) {
      locked[target] = false;
      keyDown[target] = false; }}

  // lock a key so that it cannot be triggered repeatedly
  public void ignore(int keyCode) {
    locked[keyCode] = true;
    keyDown[keyCode] = false; }
  
  // add a key listener
  public void registerKey(int keyCode) {
    int len = keyCodes.length;
    int[] _tmp = new int[len+1];
    arrayCopy(keyCodes,0,_tmp,0,len);
    _tmp[len] = keyCode;
    keyCodes = _tmp;
  }

  // check whether a key is pressed or not
  public boolean isKeyDown(int keyCode) {
    return keyDown[keyCode];
  }
  
  //public boolean noKeysDown() {
  //  for(boolean b: keyDown) { if(b) return false; }
  //  for(boolean b: locked) { if(b) return false; }
  //  return true;
  //}

  // handle key presses
  public void keyPressed(char key, int keyCode) {
    for(int i: keyCodes) {
      setIfTrue(keyCode, i); }}

  // handle key releases
  public void keyReleased(char key, int keyCode) {
    for(int i: keyCodes) {
      unsetIfTrue(keyCode, i); }}
}

InputManager inputManager = new InputManager();