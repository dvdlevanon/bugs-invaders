class Player extends GameObject
{
  final int STEP_SIZE    = 3;
  final int FADE_LENGTH  = 10;
  
  Rect screenBoundaries;
  int fadeMovement = 0;
  
  BulletsManager bulletsManager;
  int bulletResourceId;
  Level level;
  
  public Player(int resourceId, Rect position, int bulletResourceId, Rect screenBoundaries, BulletsManager bulletsManager, Level level)
  {
    super(resourceId, position, new Position(0,0));
    
    this.screenBoundaries = screenBoundaries;

    inputManager.registerKey(KEYCODE_LEFT);
    inputManager.registerKey(KEYCODE_RIGHT);
    inputManager.registerKey(KEYCODE_SPACE);
    
    this.bulletResourceId = bulletResourceId;
    this.bulletsManager = bulletsManager;
    this.level = level;
  }
  
  public void draw()
  {
    super.draw();
  }
  
  public void update()
  {
    if (fadeMovement != 0)
    {
      if (fadeMovement < 0)
      {
        moveLeft();
        fadeMovement++;
      }
      
      if (fadeMovement > 0)
      {
        moveRight();
        fadeMovement--;
      }
    }
    
    if (inputManager.isKeyDown(KEYCODE_LEFT))
    {
      moveLeft();
      slideLeft();
    }
     
    if (inputManager.isKeyDown(KEYCODE_RIGHT))
    {
      moveRight();
      slideRight();
    }
    
    if (inputManager.isKeyDown(KEYCODE_SPACE))
    {
      inputManager.ignore(KEYCODE_SPACE);
      shot();
    }
    
    super.update();
  }
  
  void movePlayerIfValid(int moveBy)
  {
    int tempX = position.x + moveBy;
    
    if ((tempX > 0) && (tempX < (screenBoundaries.width - position.width)))
    {
      position.x = tempX;
    }
  }
  
  void moveLeft()
  {
    movePlayerIfValid(-1 * STEP_SIZE);
  }
  
  void slideLeft()
  {
    fadeMovement = FADE_LENGTH * -1 * STEP_SIZE;
  }
  
  void moveRight()
  {
    movePlayerIfValid(1 * STEP_SIZE);
  }
  
  void slideRight()
  {
    fadeMovement = FADE_LENGTH * 1 * STEP_SIZE;
  }
  
  public void shot()
  {
    bulletsManager.addBullet(new Rect(position.x + (position.width / 2), position.y, 8, 9), BULLET_TRAJECTORY_UP);
  }
  
  public void kill() {
      level.removePlayer(this);
  }
}