class Bullet extends GameObject
{
  public Bullet(int resourceId, Rect position, int trajectory)
  {
    super(resourceId, position, new Position(0, trajectory));
  }
  
  public boolean didCollide(GameObject object)
  {
    return object.position.intersects(this.position);
  }
  
  public void kill() {
  }
}