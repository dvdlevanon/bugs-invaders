public class TopBar {
	GameManager gameManager;
	int topbarHeight;
	PFont font;
	int heartImageId;
	
	public TopBar(GameManager gameManager) {
		this.gameManager = gameManager;
		this.topbarHeight = 40;
		
		this.font = createFont("NimbusMonL-BoldObli", 15);
		
		this.heartImageId = resourceManager.loadResource("../processing/heart.png");
	}
	
	public int height() {
		return topbarHeight;
	}
	
	public void draw() {
		//fill(color(0, 0, 0));
		//rect(0, 0, width, this.height());
		
		textAlign(LEFT, CENTER);
		fill(255);
		textFont(font, 15);
		
		text("LEVEL: " + gameManager.getLevel(), 10, height() / 2);
		text("SCORE: " + gameManager.getScore(), 100, height() / 2);
		
		for (int i = 0; i < gameManager.getLives(); i++) {
			resourceManager.drawImage(heartImageId, new Rect(
				width - ((i + 1) * 32) - ((i + 1) * 5),
				5,
				32,
				32));
		}
	}
}
