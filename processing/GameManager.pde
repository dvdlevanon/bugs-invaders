class GameManager {
	View currentView;
	public int score = 0, lives = 3, level = 1;
	
	GameManager() {
		currentView = new SetupView(this);
		frameRate(60);
	}
	
	void draw() {
		background(0);
		currentView.draw();
	}
	
	void startGame() {
		currentView = new GameView(this);
	}

  void showMenu() {
    currentView = new SetupView(this);
  }
	public int getLives() {
		return this.lives;
	}
	
	public int getScore() {
		return this.score;
	}
	
	public int getLevel() {
		return this.level;
	}
}
