public class Button {
	String buttonText;
	int x, y, w, h, fontSize;
	PFont font;
	color normal, highlight, pressed;
	boolean mousePressedMode;
	SetupView setupView;
	
	public Button(String buttonText, int x, int y, int w, int h, int fontSize, SetupView setupView) {
		this.buttonText = buttonText;
		this.x = x;
		this.y = y;
		this.w = w;
		this.h = h;
		this.fontSize = fontSize;
		this.mousePressedMode = false;
		this.setupView = setupView;
		
		font = createFont("Georgia", fontSize, true);
	}
	
	public void setColors(color normal, color highlight, color pressed) {
		this.normal = normal;
		this.highlight = highlight;
		this.pressed = pressed;
	}
	
	public void draw() {
		boolean mouseIn = isMouseOver(mouseX, mouseY);
		
		if (mouseIn) {
			if (mousePressed) {
				fill(pressed);
				mousePressedMode = true;
			} else {
				fill(highlight);
				
				if (mousePressedMode) {
					clicked();
					mousePressedMode = false;
				}
			}
		} else {
			fill(normal);
			mousePressedMode = false;
		}
		
		rect(x, y, w, h);
		
		textFont(font, fontSize);
		textAlign(CENTER, CENTER);
		fill(0);
		text(buttonText, x + (w / 2), y + (h / 2));
	}
	
	private boolean isMouseOver(int x, int y) {
		return x > this.x && x < this.x + this.w &&
			y > this.y && y < this.y + this.h;
	}
	
	private void clicked() {
		setupView.newGameClicked();
	}
}
