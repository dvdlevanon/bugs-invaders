GameManager manager;

void setup() {
	size(1024, 768);
	manager = new GameManager();
}

void draw() {
	manager.draw();
}

void keyPressed()
{
  inputManager.keyPressed(key, keyCode);
}

void keyReleased()
{
  inputManager.keyReleased(key, keyCode);
}