class ResourceManager {
    private ArrayList<PImage> images;
    private HashMap<String, Integer> resourcesMap;

    private ResourceManager() {
        this.images = new ArrayList<PImage>();
        this.resourcesMap = new HashMap<String, Integer>();
    }

    public int loadResource(String imagePath) {
        Integer resourceId = resourcesMap.get("Image:" + imagePath);
        
        if (resourceId != null) {
          return resourceId;
        }
      
        PImage image = loadImage(imagePath);  

        if (image == null) {
            return -1;
        }
  
        images.add(image);
        resourcesMap.put("Image:" + imagePath, resourceId);
        return images.size() - 1;
    }
    
    public void drawImage(int resourceId, Rect position) {
        image(images.get(resourceId), position.x, position.y, position.width, position.height);
    }
}

ResourceManager resourceManager = new ResourceManager();