class Monster extends GameObject {
    private Level level;
    private Rect boundary;
    private int lowering;
    private int newDx;
    private int score;
    private int times;

    public Monster(Level level, int resourceId, Rect position, Rect boundary, int score) {
      super(resourceId, position, new Position(2, 0));
      this.level = level;
      this.boundary = boundary;
      this.lowering = -1; //<>//
      this.newDx = 0;
      this.score = score; //<>//
      this.times = 0;
    } //<>// //<>// //<>// //<>//

    public void update() { //<>// //<>// //<>// //<>//
        super.update();
        
        if (lowering > 0) {
          lowering--;
        }
        
        if (lowering == 0) {
          if (newDx > 0) {
            times++;
            
            if ((times % 2) == 0) {
              newDx += 1;
            }
          }
          direction.x = newDx;
          direction.y = 0;
          super.update();
          lowering = -1;
        }

        if (!boundary.isInside(position)) {
            if (boundary.y + boundary.height < position.y + position.height) {
              level.gameEnded();
              return;
            }
            direction.x *= -1;
            direction.y += 2;
            lowering = 4;
            super.update();
            newDx = direction.x;
            direction.x = 0;
        }
        
        // if killed - level.remove(this);
    }
    
    public void kill() {
      level.removeMonster(this);
    }
}