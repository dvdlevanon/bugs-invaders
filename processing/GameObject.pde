abstract class GameObject {
	  private int resourceId;
    public Rect position;
    public Position direction;

    public GameObject(int resourceId, Rect position, Position direction) {
        this.resourceId = resourceId;
        this.position = position;
        this.direction = direction;
    }

    public void update() {
        position.x += direction.x;
        position.y += direction.y;
    }

    public void draw() {
        resourceManager.drawImage(resourceId, position);
    }
    
    public abstract void kill();
}