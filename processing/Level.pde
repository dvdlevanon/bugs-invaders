class Level {
	private GameManager gameManager;
	private ArrayList<GameObject> gameObjects;
	private Rect screen;
  private int numMonsters;
  private boolean ended;
	private PFont font;
  private int framesAfterEnd;
  private int framesAfterDeath;
  private BulletsManager bulletsManager;

	public Level(GameManager gameManager, Rect screen) {
		this.gameManager = gameManager;
		this.gameObjects = new ArrayList<GameObject>();
		this.screen = screen;
    this.ended = false;
		this.font = createFont("Georgia", 72, true);
    this.framesAfterEnd = 0;
    this.framesAfterDeath = 0;

		reset();
	}

	public void reset() {
    gameObjects.clear();
		ended = false;

    regenMonsters();

    int bulletResource = resourceManager.loadResource("../processing/bullet.png");
    bulletsManager = new BulletsManager(gameObjects, screen, bulletResource);
    
    regenPlayer();
	}

  public void regenMonsters() {
    Position monsterBounds = new Position(screen.width - (6 * 40), screen.height - (3 * 42));
    this.numMonsters = 0;
		String[] monstersImages = new String[] { "../processing/scala.png", "../processing/greeny.png", "../processing/bluish.png",  "../processing/stacky.png", "../processing/stacky.png"};
    int[] monstersScores = new int[] { 10, 8, 6, 4, 4 };

    for (int i = 1; i < 6; i++)  {
      for (int j = 0; j < 7; j++) {
        Position start = new Position(screen.x + j * 40, screen.y + i * 50);

        gameObjects.add(new Monster(this, resourceManager.loadResource(monstersImages[i - 1]), new Rect(start, new Position(32, 46)), new Rect(start, monsterBounds), monstersScores[i - 1]));
        numMonsters++;
      }

      monsterBounds.y -= 50;
    }
  }
  
  public void regenPlayer() {
    this.framesAfterDeath = 0;

    if (gameManager.getLives() <= 0)
    {
      ended = true;
      return;
    }

    int playerResource = resourceManager.loadResource("../processing/player.png");
    int bulletResource = resourceManager.loadResource("../processing/bullet.png");
    Rect rect = new Rect(
        ((this.screen.width / 2) - (100 / 2)),
        (this.screen.height + this.screen.y - 22), 100, 22);
        

    Player player = new Player(playerResource, rect, bulletResource, screen, bulletsManager, this);
    
    gameObjects.add(player);
    
    bulletsManager.addPlayer(player);
  }

  public void removeMonster(Monster monster) {
    gameManager.score += monster.score;
    gameObjects.remove(monster);
    numMonsters--;
    
    if (numMonsters == 0) {
      gameManager.level++;
      gameManager.score += 32;
      regenMonsters();
    }
  }
  
  public void removePlayer(Player player) {
    gameObjects.remove(player);
    gameManager.lives--;
    framesAfterDeath++;
  }
  
  public void gameEnded() {
		if (ended == false) {
			gameManager.lives--;
		}

    ended = true;
  }

	public void update() {
    if (ended) {
      return;
    }
  
		for (GameObject object : gameObjects) {
			object.update();
		}

    bulletsManager.update();
	}

	public void draw() {
		for (GameObject object : gameObjects) {
			object.draw();
		}

    if (framesAfterDeath > 0) {
       framesAfterDeath++;
    }
    
    if (framesAfterDeath >= 60) {
      regenPlayer();
    }

    bulletsManager.draw();

		if (ended) {
			if (gameManager.getLives() <= 0) {
				textFont(font, 36 + framesAfterEnd / 2);
				textAlign(CENTER, CENTER);
				fill(255);
				text("Game End", screen.x + (screen.width / 2), screen.y + (screen.height / 2));
        framesAfterEnd++;
				
        if (framesAfterEnd > 180) {
          gameManager.showMenu();
        }
			} else {
				reset();
			}
		}
	}

	public Rect getSize() {
		return screen;
	}
}