class SetupView implements View {
	Button newButton;
	PFont headerFont;
	GameManager gameManager;
	int backgroundImage;
	
	SetupView(GameManager gameManager) {
		int buttonWidth = 200;
	    gameManager.score = 0;
	    gameManager.lives = 3;
	    gameManager.level = 1;
		
		newButton = new Button(
			"New Game",
			width / 2 - (buttonWidth / 2), 
			(height / 2) + 20, 
			buttonWidth, 
			50, 
			25,
			this);
		
		newButton.setColors(
			color(119, 247, 174),
			color(152, 255, 185),
			color(152, 255, 205));
		
		headerFont = createFont("Georgia", 40, true);
		this.gameManager = gameManager;
		
		this.backgroundImage = resourceManager.loadResource("../processing/background.png");
	}
	
	void draw() {
		resourceManager.drawImage(backgroundImage, new Rect(
				0,
				0,
				width,
				height));
		
		textFont(headerFont, 40);
		textAlign(CENTER, CENTER);
		fill(255);
		text("Bugs Invaders", width / 2, (height / 2) - 50);
		newButton.draw();
	}
	
	void newGameClicked() {
		gameManager.startGame();
	}
}