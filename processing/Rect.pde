class Rect {
    public int x;
    public int y;
    public int width;
    public int height;

    public Rect(int x, int y, int width, int height) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
    }

    public Rect(Position topLeft, Position widthHeight) {
        this.x = topLeft.x;
        this.y = topLeft.y;
        this.width = widthHeight.x;
        this.height = widthHeight.y;
    }

    public boolean isInside(Rect rect) {
        return (((x <= rect.x) && (rect.x + rect.width <= x + width)) &&
                ((y <= rect.y) && (rect.y + rect.height <= y + height)));
    }
    
    public boolean isInside(int pointX, int pointY) {
        return (((x <= pointX) && (pointX <= x + width)) &&
                ((y <= pointY) && (pointY <= y + height)));
    }
    
    public boolean intersects(Rect rect) {
      return (isInside(rect.x, rect.y) ||
              isInside(rect.x + rect.width, rect.y) ||
              isInside(rect.x, rect.y + rect.height) ||
              isInside(rect.x + rect.width, rect.y + rect.height));
    }
}